#!/usr/bin/perl 

=pod

=head1 NAME

playlist.pl - Get file paths for rhythmbox playlists.

=head1 SYNOPSIS

    ./playlist.pl [pattern]
    ./playlist.pl --list [pattern]
    ./playlist.pl --file path/to/playlists.xml

=head1 OPTIONS

=over

=item B<--list [pattern]>

Show available playlists, optionally restricted to those matching some regex.

=item B<--song-pattern pattern>

Restrict song URIs to those matching some pattern. Defaults to "file://".

=item B<--raw-uri>

Print a full URI, undecoded.

=item B<--file filename>

Specify a playlists.xml file.

=back

=head1 DESCRIPTION

Worse is better, baby.

This started at around 12 lines with a couple of regexen, and you could
probably get most of it down to a one-liner without much trouble. I decided to
add a couple of extra options instead.

=head1 BUGS

Are you kidding? I'd be shocked if this worked anywhere besides my laptop.

=head1 AUTHOR

Brennen Bearnes <bbearnes@gmail.com>
http://p1k3.com/
No Copyright

=cut

use strict;
use warnings;
use 5.10.00;

use Rhythmbox::Playlist;
use Getopt::Long qw(:config auto_help);

# Set a few defaults:
my %options = (
  file           => "$ENV{HOME}/.gnome2/rhythmbox/playlists.xml",
  'song_pattern' => 'file://',
);

my $list      = undef;
my $raw_uri   = undef;

GetOptions(
  'file=s'         => \$options{'file'},
  'song-pattern=s' => \$options{'song_pattern'},

  'list'           => \$list,
  'raw-uri'        => \$raw_uri,
);

my $lists = Rhythmbox::Playlist->new(%options);

# Take patterns from whatever's left in arguments:
my @patterns = @ARGV;
$patterns[0] //= '.*';

for my $pattern (@patterns) {
  if ($list) {
    print join "\n", $lists->lists($pattern);
  } else {
    if ($raw_uri) {
      print join "\n", $lists->songs($pattern);
    } else {
      print join "\n", $lists->songs_decoded($pattern);
    }
  }
}
