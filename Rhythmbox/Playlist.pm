package Rhythmbox::Playlist;

use strict;
use warnings;

use XML::Simple;
use URI::Escape qw(uri_unescape);

sub new {
  my $class = shift;
  my %params = @_;
  my $self = \%params;
  bless $self, $class;
  
  $self->read_file();

  return $self;
}

sub read_file {
  my $self = shift;
  # Get XML data:
  my $xml = new XML::Simple;
  my $data = $xml->XMLin($self->{file});
  $self->{playlists} = $data->{playlist};
}

sub lists {
  my $self = shift;
  my ($pattern) = @_;
  my @names = keys %{ $self->{playlists} };
  return sort grep { m/$pattern/ } @names;
}

sub songs_decoded {
  my $self = shift;
  my @songs = $self->songs(@_);

  foreach my $song (@songs) {
    $song =~ s{ (?:file|smb|ssh|sftp) :// (.*) }{$1}x;
    $song = uri_unescape($song);
  }

  return @songs;
}

sub songs {
  my $self = shift;
  my ($pattern) = @_;

  my @names = grep { m/$pattern/ }
              keys %{ $self->{playlists} };

  my @songs;
  foreach my $name (@names) {
    my $locations = $self->{playlists}->{$name}{'location'};
    next unless defined $locations;
    push @songs, (ref($locations) ? @{ $locations } : $locations);
  }

  @songs = sort grep { m/$self->{song_pattern}/ } @songs;

  return @songs;
}

1;
